import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'https://burger-builder-course-react.firebaseio.com/'
});

export default axiosInstance;