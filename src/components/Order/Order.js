import React from 'react';
import './Order.scss';

const Order = (props) => {
    const ingredients = [];

    for (let ingredientName in props.ingredients) {
        ingredients.push({
            name: ingredientName,
            amount: props.ingredients[ingredientName]
        });
    }

    const ingredientOutput = ingredients.map(ingr => {
        return (
            <span 
                className="order__ingredient"
                key={ingr.name}>
                {ingr.name} ({ingr.amount})
            </span>
        );
    });

    return (
        <div className="order">
            <p>Ingredients: {ingredientOutput}</p>
            <p>Price: <strong>{+props.totalPrice}$</strong></p>
        </div>
    );
};

export default Order;