import React from 'react';
import Burger from '../../Burger/Burger';
import './CheckoutSummary.scss';
import ButtonDefault from '../../UI/ButtonDefault/ButtonDefault';

const CheckoutSummary = (props) => {
    return (
        <div className="checkout-summary">
            <h1>We hope you'll find the burger delicious!</h1>
            <div>
                <Burger ingredients={props.ingredients} />
            </div>
            <ButtonDefault
                cls="checkout-summary__btn btn-default--strong btn-default--danger"
                clicked={props.onCheckoutCancel}>Cancel
            </ButtonDefault>
            <ButtonDefault
                cls="checkout-summary__btn btn-default--strong btn-default--success"
                clicked={props.onCheckoutContinue}>Continue
            </ButtonDefault>
        </div>
    );
}
 
export default CheckoutSummary;