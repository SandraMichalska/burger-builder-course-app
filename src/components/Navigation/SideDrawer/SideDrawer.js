import React from 'react';
import './SideDrawer.scss';

import Logo from '../../UI/Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Backdrop from '../../UI/Backdrop/Backdrop';

const SideDrawer = (props) => {
    const sideDrawerCls = props.showSideDrawer ? "side-drawer--open" : "side-drawer--close";

    return (
        <>
            <Backdrop 
                showBackdrop={props.showSideDrawer}
                clicked={props.onSideDrawerClose} />
            <div className={`side-drawer ${sideDrawerCls}`}>
                <div>
                    <Logo clsLogo="logo--side-drawer" clsLogoImg="logo__img--side-drawer"/>
                </div>
                <nav>
                    <NavigationItems clicked={props.onSideDrawerClose}/>
                </nav>
            </div>
        </>
    );
};

export default SideDrawer;