import React from 'react';
import './SideDrawerToggle.scss';

const SideDrawerToggle = (props) => (
    <div
        onClick={props.clicked}
        className="side-drawer-toggle">
        <div className="side-drawer-toggle__part"></div>
        <div className="side-drawer-toggle__part"></div>
        <div className="side-drawer-toggle__part"></div>
    </div>
);

export default SideDrawerToggle;
