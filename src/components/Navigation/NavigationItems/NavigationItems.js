import React from 'react';
import './NavigationItems.scss';
import { NavLink } from 'react-router-dom';

const NavigationItems = (props) => (
    <ul className="nav-items">
        <li className="nav-items__item">
            <NavLink 
                activeClassName="nav-items__link--active"
                className="nav-items__link" 
                onClick={props.clicked}
                to="/" exact>Burger builder
            </NavLink>
        </li>
        <li className="nav-items__item">
            <NavLink 
                activeClassName="nav-items__link--active"
                className="nav-items__link"
                onClick={props.clicked}
                to="/orders">Orders
            </NavLink>
        </li>
    </ul>
);

export default NavigationItems;