import React from 'react';
import './Toolbar.scss';
import Logo from '../../UI/Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import SideDrawerToggle from '../SideDrawer/SideDrawerToggle/SideDrawerToggle';

const Toolbar = (props) => {
    return (
        <header className="toolbar">
            <SideDrawerToggle clicked={props.onSideDrawerToggle}/>
            <div className="toolbar__logo-wrap">
                <Logo clsLogo="" clsLogoImg=""/>
            </div>
            <nav className="nav nav--hide-on-mobile">
                <NavigationItems />
            </nav>
        </header>
    );
};

export default Toolbar;