import React from 'react';
import { NavLink } from 'react-router-dom';

import './Logo.scss';
import logoImg from '../../../assets/images/logo.png';

const Logo = (props) => (
    <NavLink to="/" exact>
        <div className={`logo ${props.clsLogo}`}>
            <img className={`logo__img ${props.clsLogoImg}`} src={logoImg} alt="Burger logo" />
        </div>
    </NavLink>
);

export default Logo;