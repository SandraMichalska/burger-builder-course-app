import React from 'react';
import './Backdrop.scss';

const Backdrop = (props) => (
    props.showBackdrop ? <div className="backdrop" onClick={props.clicked}></div> : null
);

export default Backdrop;
