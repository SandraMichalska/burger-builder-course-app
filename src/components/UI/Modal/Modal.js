import React from 'react';
import './Modal.scss';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends React.Component {
    style = null;

    componentDidMount() {
        window.addEventListener("orientationchange", () => {
            this.centerModalVertically(this.props.thisModalCls);
        }, false);
    }

    centerModalVertically = (modalCls) => {
        const modal = document.querySelector(`.${modalCls}`);

        if(!modal) {
            return;
        }
        
        const DEVICE_HEIGHT = window.innerHeight; // not outerHeight!
        // const MODAL_HEIGHT = document.querySelector(".modal").clientHeight; // more than just viewport!
        const WINDOW_STYLES = window.getComputedStyle(modal, null);
        
        const MODAL_HEIGHT = WINDOW_STYLES.getPropertyValue("height");
        const modalTop = ((DEVICE_HEIGHT - parseInt(MODAL_HEIGHT)) / 2);

        // console.log(DEVICE_HEIGHT, "\n", WINDOW_STYLES, "\n", MODAL_HEIGHT, "\n", modalTop);
        
        this.style = {
            top: modalTop + "px"
        };
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.showBackdrop !== this.props.showBackdrop
        || nextProps.children !== this.props.children;
    }

    // componentWillUpdate() {
    //     console.log('Modal update');
    // }

    render() { 
        if(this.props.showBackdrop)
            this.centerModalVertically(this.props.thisModalCls);

        const showBackdropCls = this.props.showBackdrop ? "modal--show" : "";
        
        return (
            <>
                <Backdrop 
                    showBackdrop={this.props.showBackdrop} 
                    clicked={this.props.onCancelPurchase} />
                <div
                    style={this.style}
                    className={`modal ${this.props.thisModalCls} ${showBackdropCls}`}>
                    {this.props.children}
                </div>
            </>
        );
    }
};

export default Modal;
