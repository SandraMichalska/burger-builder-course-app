import React from 'react';
import './ButtonModal.scss';

const ButtonModal = (props) => (
    <button 
        onClick={props.clicked}
        className={`btn-modal ${props.cls}`}>
        {props.children}
    </button>
);

export default ButtonModal;
