import React from 'react';
import './Input.scss';

class Input extends React.Component {
    render() {
        let inputElement;
        let invalidCls = '';
            
        if(this.props.invalid && this.props.touched) {
            invalidCls = "input__elem--invalid";
        }

        switch(this.props.elementType) {
            case('input'):
                let ref;
                let pattern;
                
                // Chrome problem, as described in ContactData.js
                // if(this.props.elementConfig.type === "email") {
                //     ref = this.props.emailInputRef;
                if(this.props.element === "email") {
                    ref = this.props.emailInputRef;
                    pattern = "^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$";
                } else if(this.props.element === "zipCode") {
                    ref = this.props.zipcodeInputRef;
                    pattern = "^\\d+([ \\-]\\d+)?";
                }

                inputElement = <input
                    className={`input__elem ${invalidCls}`} 
                    {...this.props.elementConfig}
                    value={this.props.value}
                    ref={ref}
                    pattern={pattern}
                    onChange={this.props.changed}/>;
                break;
            case('select'):
                inputElement = (
                    <select 
                        className="input__elem"
                        value={this.props.value}
                        ref="select"
                        onChange={this.props.changed}>
                        {this.props.elementConfig.options.map(option => (
                            <option 
                                className="input__select-option" 
                                key={option.value} 
                                value={option.value}>
                                {option.displayValue}
                            </option>
                        ))}
                    </select>
                );
                break;
            default:
                inputElement = <textarea 
                    className="input__elem" 
                    {...this.props.elementConfig} 
                    value={this.props.value}
                    onChange={this.props.changed} />;
        }

        return (
            <div className="input">
                <label className="input__label">{this.props.label}</label>
                {inputElement}
            </div>
        );
    }
};
 
export default Input;