import React from 'react';
import './ButtonDefault.scss';

const ButtonDefault = (props) => (
    <button 
        onClick={props.clicked}
        className={`btn-default ${props.cls}`}
        disabled={props.disabled}>
        {props.children}
    </button>
);

export default ButtonDefault;