import React from 'react';
import ButtonModal from '../../UI/ButtonModal/ButtonModal';
import '../../UI/ButtonModal/ButtonModal.scss';
import './OrderSummary.scss';

// a class component only to see how shouldComponentUpdate doesn't rerender this component when not needed
class OrderSummary extends React.Component {
    // componentWillUpdate() {
    //     console.log('OrderSummary update');
    // }

    render() {
        const ingredients = Object.keys(this.props.ingredients)
            .map((ingKey) => {
                if(this.props.ingredients[ingKey] <= 0)
                    return null;

                return <li key={ingKey} className="orderSummary__list-item">
                    {ingKey} x {this.props.ingredients[ingKey]}
                </li>;
            });

        return (
            <div className="orderSummary">
                <h2 className="orderSummary__heading">Order summary</h2>
                <p>Your delicious burger with the following ingredients:</p>
                <ul>
                    {ingredients}
                </ul>
                <p>Total price: {this.props.totalPrice}$</p>
                <div className="orderSummary__btn-wrap">
                    <ButtonModal 
                        clicked={this.props.onCancelPurchase}
                        cls="btn-modal--danger">
                        Cancel
                    </ButtonModal>
                    <ButtonModal 
                        clicked={this.props.onContinuePurchase}
                        cls="btn-modal--success">
                        Continue
                    </ButtonModal>
                </div>
            </div>
        );
    }
}

export default OrderSummary;
