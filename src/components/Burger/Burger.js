import React from 'react';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import './Burger.scss';

const Burger = (props) => {
    let allIngredients = Object.keys(props.ingredients).map(ingKey => {
        return [...Array(props.ingredients[ingKey])].map( (_, i) => {
            return <BurgerIngredient key={ingKey + i} type={ingKey} />;
        });
    })
    .reduce((arr, elem) => {
        return arr.concat(elem)
    }, []);

    if(allIngredients.length === 0) {
        allIngredients = <p className="burger__message">Please add some ingredients!</p>;
    } 

    return (
        <div className="burger">
            <BurgerIngredient type="bread-top" />
            {allIngredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
};

export default Burger;