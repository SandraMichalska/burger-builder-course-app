import React from 'react';
import './BuildControl.scss';
import ButtonDefault from '../../../UI/ButtonDefault/ButtonDefault';
import '../../../UI/ButtonDefault/ButtonDefault.scss';

const BuildControl = (props) => {
    return (
        <div className="build-control">
            <p className="build-control__label"><strong>{props.ingredientLabel}</strong> ({props.ingredientPrice}$)</p>
            <ButtonDefault
                cls="build-control__btn"
                clicked={props.onAddClick}>
                Add
            </ButtonDefault>
            <ButtonDefault
                cls="build-control__btn"
                clicked={props.onRemoveClick}
                disabled={props.isRemovingDisabled}>
                Remove
            </ButtonDefault>
        </div>
    );
};

export default BuildControl;