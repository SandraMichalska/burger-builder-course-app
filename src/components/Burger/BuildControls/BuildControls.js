import React from 'react';
import './BuildControls.scss';
import BuildControl from './BuildControl/BuildControl';
import ButtonDefault from '../../UI/ButtonDefault/ButtonDefault';
import '../../UI/ButtonDefault/ButtonDefault.scss';

const controls = [
    { label: 'Cheese', type: 'cheese'},
    { label: 'Meat', type: 'meat'},
    { label: 'Bacon', type: 'bacon'},
    { label: 'Salad', type: 'salad'}
];

const BuildControls = (props) => {
    controls.map(control => {
        return control.price = props.ingredientPrices[control.type];
    });

    return (
        <div className="build-controls">
            <p className="build-controls__price">Total price: {props.totalPrice}$</p>
            {controls.map(control => {
                return <BuildControl 
                    key={control.label}
                    ingredientLabel={control.label}
                    ingredientPrice={control.price}
                    onAddClick={() => props.onAddClick(control.type)}
                    onRemoveClick={() => props.onRemoveClick(control.type)}
                    isRemovingDisabled={props.isRemovingDisabled[control.type]} />
            })}
            <ButtonDefault
                cls="build-controls__btn btn-default--strong"
                clicked={props.onOrderBtnClick}
                disabled={!props.purchaseable}>
                Order now
            </ButtonDefault>
        </div>
    );
};

export default BuildControls;