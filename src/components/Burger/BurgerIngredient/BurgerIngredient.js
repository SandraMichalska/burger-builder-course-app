import React from 'react';
import PropTypes from 'prop-types';

class BurgerIngredient extends React.Component {
    render() {
        let ingredient = null;

        switch(this.props.type) {
            case "bread-top":
                ingredient = (
                    <div className="burger__bread-top">
                        <div className="burger__seed1"></div>
                        <div className="burger__seed2"></div>
                    </div>
                );
                break;
            case "bread-bottom":
                ingredient = <div className="burger__bread-bottom"></div>;
                break;
            case "meat":
                ingredient = <div className="burger__meat"></div>;
                break;
            case "cheese":
                ingredient = <div className="burger__cheese"></div>;
                break;
            case "bacon":
                ingredient = <div className="burger__bacon"></div>;
                break;
            case "salad":
                ingredient = <div className="burger__salad"></div>;
                break;
            default:
                ingredient = null;
        }

        return ingredient;
    }
}

BurgerIngredient.propTypes = {
    type: PropTypes.string.isRequired
};

export default BurgerIngredient;