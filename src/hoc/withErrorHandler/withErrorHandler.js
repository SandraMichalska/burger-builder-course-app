import React from 'react';
import Modal from '../../components/UI/Modal/Modal';
import Aux from '../Aux/Aux';

const withErrorHandler = (WrappedComponent, axiosInstance) => {
    return class extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                error: null
            }

            this.resInterceptor = axiosInstance.interceptors.response.use(res => res, error => {
                this.setState({
                    error: error
                });
            });

            this.reqInterceptor = axiosInstance.interceptors.request.use(req => {
                this.setState({
                    error: null
                });

                return req;
            });
        }

        handleErrorConfirm = () => {
            this.setState({
                error: null
            });
        }

        componentWillUnmount() {
            axiosInstance.interceptors.response.eject(this.resInterceptor);
            axiosInstance.interceptors.request.eject(this.reqInterceptor);
        }

        render() {
            return (
                <Aux>
                    <Modal 
                        thisModalCls="modal-error"
                        showBackdrop={this.state.error}
                        onCancelPurchase={this.handleErrorConfirm}>
                        { this.state.error ? this.state.error.message : null }
                    </Modal>
                    <WrappedComponent {...this.props} />
                </Aux>
            );
        };
    }
};

export default withErrorHandler;