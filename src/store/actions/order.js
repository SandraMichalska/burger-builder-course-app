import * as actionTypes from './actionTypes';
import axiosInstance from '../../axios';

// purchasing a burger
export const purchaseInit = () => {
    return {
        type: actionTypes.PURCHASE_INIT
    }
};

export const purchaseBurgerStart = () => {
    return {
        type: actionTypes.PURCHASE_BURGER_START
    }
};

export const purchaseBurgerSuccess = (orderId, orderData) => {
    return {
        type: actionTypes.PURCHASE_BURGER_SUCCESS,
        orderId: orderId,
        orderData: orderData
    }
};

export const purchaseBurgerFailure = (error) => {
    return {
        type: actionTypes.PURCHASE_BURGER_FAILURE,
        error: error
    }
};

export const purchaseBurger = (orderData) => {
    // async code thanks to redux-thunk
    return dispatch => {
        dispatch(purchaseBurgerStart());

        axiosInstance.post('/orders.json', orderData)
            .then(response => {
                dispatch(purchaseBurgerSuccess(response.data.name, orderData));
            }).catch(error => {
                dispatch(purchaseBurgerFailure(error));
            });
    }
};

// fetching orders
export const fetchOrdersStart = () => {
    return {
        type: actionTypes.FETCH_ORDERS_START
    }
};

export const fetchOrdersSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDERS_SUCCESS,
        orders: orders
    }
};

export const fetchOrdersFailure = (error) => {
    return {
        type: actionTypes.FETCH_ORDERS_FAILURE,
        error: error
    }
};

export const fetchOrders = () => {
    // async code thanks to redux-thunk
    return dispatch => {
        dispatch(fetchOrdersStart());
        axiosInstance.get('/orders.json')
            .then(response => {
                const orders = [];
                for(let key in response.data) {
                    orders.push({
                        id: key,
                        ...response.data[key]
                    });
                }
                
                dispatch(fetchOrdersSuccess(orders));
            }).catch(error => {
                dispatch(fetchOrdersFailure(error));
            });
    }
};