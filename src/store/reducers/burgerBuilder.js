import * as actionTypes from './../actions/actionTypes';
import { INGREDIENT_PRICES } from '../../containers/BurgerBuilder/BurgerBuilder';

const initialTotalPrice = 3;

const initialState = {
    ingredients: null,
    totalPrice: initialTotalPrice,
    error: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ADD_INGREDIENT:
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingredientName]: state.ingredients[action.ingredientName] + 1
                },
                totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName]
            };
        case actionTypes.REMOVE_INGREDIENT:
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingredientName]: state.ingredients[action.ingredientName] - 1
                },
                totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName]
            };
        case actionTypes.SET_INGREDIENTS:
            return {
                ...state,
                ingredients: {
                    // to keep the right order of ingredients
                    cheese: action.ingredients.cheese,
                    meat: action.ingredients.meat,
                    bacon: action.ingredients.bacon,
                    salad: action.ingredients.salad
                },
                totalPrice: initialTotalPrice,
                error: false
            }
        case actionTypes.FETCH_INGREDIENTS_FAILED:
            return {
                ...state,
                error: true
            }
        default:
            return state;
    }
};

export default reducer;