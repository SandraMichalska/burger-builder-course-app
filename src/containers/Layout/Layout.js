import React from 'react';
import Aux from '../../hoc/Aux/Aux';
import './Layout.scss';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

class Layout extends React.Component {
    state = {
        showSideDrawer: false
    };
    
    handleSideDrawerClose = () => {
        this.setState({
            showSideDrawer: false
        });
    }

    handleSideDrawerToggle = () => {
        this.setState({
            showSideDrawer: true
        });
    }

    render() {
        return (
            <Aux>
                <SideDrawer
                    onSideDrawerClose={this.handleSideDrawerClose}
                    showSideDrawer={this.state.showSideDrawer} />
                <Toolbar onSideDrawerToggle={this.handleSideDrawerToggle} />
                <main className="main">
                    {this.props.children}
                </main>
            </Aux>
        );
    }
};

export default Layout;