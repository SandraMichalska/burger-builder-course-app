import React from 'react';
import { connect } from 'react-redux';

import Aux from '../../hoc/Aux/Aux';
import './BurgerBuilder.scss';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import axiosInstance from '../../axios';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index';

export const INGREDIENT_PRICES = {
    cheese: 2,
    meat: 5,
    bacon: 4,
    salad: 3
};

class BurgerBuilder extends React.Component {
    state = {
        purchasing: false
    }

    componentDidMount() {
        this.props.onInitIngredients();
    }

    updatePurchaseable() {
        const ingredients = {
            ...this.props.ingredients
        };

        const sum = Object.keys(ingredients)
            .map(ingKey => {
                return ingredients[ingKey]
            }).reduce((sum, elem) => {
                return sum + elem;
            }, 0);
            
        return sum > 0;
    }

    handleOrderBtnClick = () => {
        this.setState({
            purchasing: true
        });
    }

    handleCancelPurchase = () => {
        this.setState({
            purchasing: false
        });
    }

    handleContinuePurchase = () => {
        this.props.onPurchaseInit();
        this.props.history.push('/checkout');
    }

    render() {
        let isRemovingDisabled = {
            ...this.props.ingredients
        };

        for(let ingKey in isRemovingDisabled) {
            isRemovingDisabled[ingKey] = isRemovingDisabled[ingKey] <= 0;
        }

        let orderSummary = null; 
        let burger = this.props.error 
            ? <p className="burger-error">Cannot load the ingredients.</p> 
            : <Spinner />;

        if(this.props.ingredients) {
            burger = (
                <>
                    <Burger ingredients={this.props.ingredients} />
                    <BuildControls
                        onAddClick={this.props.onAddIngredient}
                        onRemoveClick={this.props.onRemoveIngredient}
                        isRemovingDisabled={isRemovingDisabled}
                        totalPrice={this.props.totalPrice}
                        purchaseable={this.updatePurchaseable()}
                        ingredientPrices={INGREDIENT_PRICES}
                        onOrderBtnClick={this.handleOrderBtnClick} />
                </>
            );

            orderSummary =
                <OrderSummary 
                    ingredients={this.props.ingredients}
                    totalPrice={this.props.totalPrice}
                    onContinuePurchase={this.handleContinuePurchase}
                    onCancelPurchase={this.handleCancelPurchase} />;
        }

        return (
            <Aux>
                <Modal
                    thisModalCls="modal--order" 
                    showBackdrop={this.state.purchasing} 
                    onCancelPurchase={this.handleCancelPurchase} >
                    {orderSummary}
                </Modal>
                {burger}
            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
       ingredients: state.burgerBuilder.ingredients,
       totalPrice: state.burgerBuilder.totalPrice,
       error: state.burgerBuilder.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddIngredient: (ingrName) => dispatch(actions.addIngredient(ingrName)),
        onRemoveIngredient: (ingrName) => dispatch(actions.removeIngredient(ingrName)),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onPurchaseInit: () => dispatch(actions.purchaseInit())
    }   
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axiosInstance));