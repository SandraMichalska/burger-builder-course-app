import React from 'react';
import { connect } from 'react-redux';

import ButtonDefault from '../../../components/UI/ButtonDefault/ButtonDefault.js';
import Input from '../../../components/UI/Input/Input';
import './ContactData.scss';
import Spinner from '../../../components/UI/Spinner/Spinner';
import axiosInstance from '../../../axios';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../../store/actions/index';

class ContactData extends React.Component {
    constructor(props) {
        super(props);
        this.emailInputRef = React.createRef();
        this.zipcodeInputRef = React.createRef();   
    }

    state = {
        orderForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    maxLength: 80
                },
                valid: false,
                touched: false
            },
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Street'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    maxLength: 80
                },
                valid: false,
                touched: false
            },
            zipCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'ZIP Code'
                },
                value: '',
                validation: {
                    required: true,
                    maxLength: 80
                },
                valid: false,
                touched: false
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Country'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    maxLength: 80
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    // type text + pattern instead of email to avoid the Chrome problem with the cursor going back to the beginning of the field when deleting a space entered...
                    type: 'text',
                    placeholder: 'Your E-Mail'
                },
                value: '',
                validation: {
                    required: true,
                    maxLength: 80
                },
                valid: false,
                touched: false
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: 'fastest', displayValue: 'Fastest'},
                        {value: 'cheapest', displayValue: 'Cheapest'}
                    ]
                },
                value: 'fastest'
            }
        },
        isFormValid: false
    };

    handleOrder = (e) => {
        e.preventDefault();

        const formData = {};

        for(let formElementIdentifier in this.state.orderForm) {
            formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value;
        }

        const order = {
            ingredients: this.props.ingredients,
            totalPrice: this.props.totalPrice,
            orderData: formData
        };

        this.props.onBurgerOrder(order);
    }

    checkValidity = (inputIdentifier, inputData) => {
        let isValid = true;

        if(inputData.validation.required) {
            isValid = inputData.value.trim() !== '' && isValid;
        }

        if(inputData.validation.minLength) {
            isValid = inputData.value.length >= inputData.validation.minLength && isValid;
        }

        if(inputData.validation.maxLength) {
            isValid = inputData.value.length <= inputData.validation.maxLength && isValid;
        }
                
        if(inputIdentifier === 'email') {
            // isValid = !this.emailInputRef.current.validity.typeMismatch && isValid; // Chrome problem
            isValid = !this.emailInputRef.current.validity.patternMismatch && isValid;
        }

        if(inputIdentifier === 'zipCode') {
            isValid = !this.zipcodeInputRef.current.validity.patternMismatch && isValid;
        }

        return isValid;
    }

    handleInputChange(event, inputIdentifier) {
        // Clone the object DEEPLY, and not just copy the pointers to inner objects!
        const updatedOrderForm = {
            ...this.state.orderForm
        };

        const updatedFormElement = { 
            ...updatedOrderForm[inputIdentifier]
        };
        
        updatedFormElement.value = event.target.value;

        if(inputIdentifier !== "deliveryMethod") {
            updatedFormElement.valid = this.checkValidity(inputIdentifier, updatedFormElement);
            updatedFormElement.touched = true;
        }

        updatedOrderForm[inputIdentifier] = updatedFormElement;

        let isFormValid = true;

        for(let inputIdentifier in updatedOrderForm) {
            if(inputIdentifier === "deliveryMethod") continue;
            isFormValid = updatedOrderForm[inputIdentifier].valid && isFormValid;
        }

        this.setState({
            orderForm: updatedOrderForm,
            isFormValid: isFormValid
        });
    }

    render() { 
        const formElementsArray = [];

        for (let key in this.state.orderForm) {
            formElementsArray.push({
                id: key,
                config: this.state.orderForm[key]
            });
        }

        let form = (
            <form
                onSubmit={this.handleOrder} 
                className="contact-data__form">
                {formElementsArray.map(formElement => {
                    return <Input 
                        key={formElement.id}
                        element={formElement.id}
                        elementType={formElement.config.elementType}
                        emailInputRef={
                            formElement.id === "email"
                                ? this.emailInputRef
                                : undefined
                        }
                        zipcodeInputRef={
                            formElement.id === "zipCode"
                                ? this.zipcodeInputRef
                                : undefined
                        }
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        touched={formElement.config.touched}
                        changed={(event) => this.handleInputChange(event, formElement.id)} />;
                })}

                <ButtonDefault 
                    cls="contact-data__btn btn-default--strong"
                    disabled={!this.state.isFormValid}>
                    Order
                </ButtonDefault>
            </form>
        );

        if(this.props.loading)
            form = <Spinner />;

        return (
            <div className="contact-data">
                <h2 className="contact-data__heading">Please enter your contact data</h2>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
       ingredients: state.burgerBuilder.ingredients,
       totalPrice: state.burgerBuilder.totalPrice,
       loading: state.order.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onBurgerOrder: (orderData) => dispatch(actions.purchaseBurger(orderData)) 
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData, axiosInstance));