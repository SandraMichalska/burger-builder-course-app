import React from 'react';
import { connect } from 'react-redux';

import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import { Route, Redirect } from 'react-router-dom';
import ContactData from './ContactData/ContactData';

class Checkout extends React.Component {
    handleCheckoutCancel = () => {
        this.props.history.goBack();
    }

    handleCheckoutContinue = () => {
        this.props.history.replace('/checkout/contact-data');
    }

    render() {
        let checkoutSummary = <Redirect to='/'/>;
        
        if(this.props.ingredients) {
            const purchasedRedirect = this.props.purchased ? <Redirect to='/'/> : null;

            checkoutSummary = (
                <div>
                    {purchasedRedirect}
                    <CheckoutSummary 
                        ingredients={this.props.ingredients}
                        onCheckoutCancel={this.handleCheckoutCancel}
                        onCheckoutContinue={this.handleCheckoutContinue} />
                    <Route 
                        path={this.props.match.path + '/contact-data'} 
                        component={ContactData} />
                </div>
            );
        };

        return checkoutSummary;
    }
}

const mapStateToProps = state => {
    return {
       ingredients: state.burgerBuilder.ingredients,
       purchased: state.order.purchased
    };
};

export default connect(mapStateToProps)(Checkout);
