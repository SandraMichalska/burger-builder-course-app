import React, { Component } from 'react';
import { connect } from 'react-redux';

import Order from '../../components/Order/Order';
import axiosInstance from '../../axios';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../components/UI/Spinner/Spinner';
import './Orders.scss';
import * as actions from '../../store/actions/index';

class Orders extends Component {
    componentDidMount() {
        this.props.onFetchOrders();
    }

    render () {
        let orders = <Spinner />;

        if(!this.props.loading) {
            orders = this.props.orders.reverse().map(order => 
                <Order 
                    key={order.id} 
                    ingredients={order.ingredients} 
                    totalPrice={order.totalPrice} />
            );
        }

        if(this.props.error) {
            orders = <p className="order-error">Cannot load the orders.</p>;
        }

        return (
            <div>
                {orders}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        orders: state.order.orders,
        loading: state.order.loading,
        error: state.order.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchOrders: () => dispatch(actions.fetchOrders())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Orders, axiosInstance));